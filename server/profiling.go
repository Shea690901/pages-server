package server

import (
	"net/http"
	_ "net/http/pprof"

	"github.com/rs/zerolog/log"
)

func StartProfilingServer(listeningAddress string) {
	server := &http.Server{
		Addr:    listeningAddress,
		Handler: http.DefaultServeMux,
	}

	log.Info().Msgf("Starting debug server on %s", listeningAddress)

	go func() {
		log.Fatal().Err(server.ListenAndServe()).Msg("Failed to start debug server")
	}()
}
