package upstream

import (
	"testing"
)

func TestRedirect_rewriteURL(t *testing.T) {
	for _, tc := range []struct {
		redirect   Redirect
		reqURL     string
		wantDstURL string
		wantOk     bool
	}{
		{Redirect{"/", "/dst", 200}, "/", "/dst", true},
		{Redirect{"/", "/dst", 200}, "/foo", "", false},
		{Redirect{"/src", "/dst", 200}, "/src", "/dst", true},
		{Redirect{"/src", "/dst", 200}, "/foo", "", false},
		{Redirect{"/src", "/dst", 200}, "/src/foo", "", false},
		{Redirect{"/*", "/dst", 200}, "/", "/dst", true},
		{Redirect{"/*", "/dst", 200}, "/src", "/dst", true},
		{Redirect{"/src/*", "/dst/:splat", 200}, "/src", "/dst/", true},
		{Redirect{"/src/*", "/dst/:splat", 200}, "/src/", "/dst/", true},
		{Redirect{"/src/*", "/dst/:splat", 200}, "/src/foo", "/dst/foo", true},
		{Redirect{"/src/*", "/dst/:splat", 200}, "/src/foo/bar", "/dst/foo/bar", true},
		{Redirect{"/src/*", "/dst/:splatsuffix", 200}, "/src/foo", "/dst/foosuffix", true},
		{Redirect{"/src/*", "/dst:splat", 200}, "/src/foo", "/dstfoo", true},
		{Redirect{"/src/*", "/dst", 200}, "/srcfoo", "", false},
		// This is the example from FEATURES.md:
		{Redirect{"/articles/*", "/posts/:splat", 302}, "/articles/2022/10/12/post-1/", "/posts/2022/10/12/post-1/", true},
	} {
		if dstURL, ok := tc.redirect.rewriteURL(tc.reqURL); dstURL != tc.wantDstURL || ok != tc.wantOk {
			t.Errorf("%#v.rewriteURL(%q) = %q, %v; want %q, %v",
				tc.redirect, tc.reqURL, dstURL, ok, tc.wantDstURL, tc.wantOk)
		}
	}
}
